var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;
  mongoose = require('mongoose'),
  Supplier = require('./api/models/supplier'), //created model loading here
  bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_CONNECTION || 'mongodb://localhost/Collectordb');

// app.use(function(req, res) {
//   res.status(404).send({url: req.originalUrl + ' not found'})
// });
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/config/routes'); //importing route
routes(app); //register the route

app.listen(port);

console.log('Listening on PORT: ' + port);
