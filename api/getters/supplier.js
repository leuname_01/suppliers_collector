async function supplierInfo(page){
  const name = await page.$eval('#sellerName', el => el.innerText)
  if( name.search(/amazon/i) != -1) return null

  var supplier = { name: name, stats: {}}

  console.log('[Seller_extractor]: Supplier', name);

  const tableRows = await page.$$eval('#feedback-summary-table tr', table => table.slice(1,5).map((row)=> row.innerText.split('\t')))

  tableRows.forEach((row) => {
    supplier.stats[row[0].toLowerCase()] = {
      one_month: parseFloat(row[1].replace(',', '')) || null,
      three_months: parseFloat(row[2].replace(',', '')) || null,
      one_year: parseFloat(row[3].replace(',', '')) || null,
      lifetime: parseFloat(row[4].replace(',', '')) || null
    }
  })
  return supplier
}

module.exports = {
  supplierInfo
}
