'use strict';
module.exports = function(app) {
  var supplierController = require('../controllers/suppliersController');

  app.route('/')
    .get(supplierController.show_all)
    .post(supplierController.create_one)
    // .delete(supplierController.delete_all);

  app.route('/search')
    .get(supplierController.search_it)


  app.route('/supplier/:supplierId')
    .get(supplierController.show_it)
    .put(supplierController.update_it)
    .delete(supplierController.delete_it);
};
