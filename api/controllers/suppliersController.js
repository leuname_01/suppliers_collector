'use strict';

var mongoose = require('mongoose'),
  Supplier = mongoose.model('Supplier');

exports.show_all = function(req, res) {
  Supplier.find({}, function(err, suppliers) {
    if (err) res.send(err);
    res.json({length: suppliers.length, suppliers});
  });
};

exports.create_one = function(req, res) {
  var new_supplier = new Supplier(req.body);
  new_supplier.save(function(err, supplier) {
    if (err) res.send(err);
    res.json(supplier);
  });
};

exports.delete_all = function(req, res){
  Supplier.remove({}, function(err){
    if (err) res.send(err);
    res.json({ message: 'All Suppliers were successfully deleted' });
  })
}

exports.show_it = function(req, res) {
  Supplier.findById(req.params.supplierId, function(err, supplier) {
    if (err) res.send(err);
    res.json(supplier);
  });
};

exports.update_it = function(req, res) {
  Supplier.findOneAndUpdate(
    {_id: req.params.supplierId},
    req.body,
    {new: true},
    function(err, supplier) {
      if (err) res.send(err);
      res.json(supplier);
    }
  );
};

exports.delete_it = function(req, res) {
  Supplier.remove({
    _id: req.params.supplierId
  }, function(err, supplier) {
    if (err) res.send(err);
    res.json({ message: 'Supplier was successfully deleted' });
  });
};

exports.search_it = function(req, res) {
  console.log(req.query);
  const query = req.query
  Supplier.find({
      category: new RegExp(query.category, 'i'),
      subcategory: new RegExp(query.subcategory, 'i')
    }, function(err, suppliers) {
    if (err) res.send(err);
    res.json({length: suppliers.length, suppliers});
  });

}
