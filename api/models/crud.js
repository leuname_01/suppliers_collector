const getter = require('../getters/supplier')

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/Collectordb')
const Supplier = require('./supplier')

async function createSupplier(supplier_data){
  var new_supplier = new Supplier(supplier_data)
  new_supplier.save(function(err, supplier) {
    if (err) console.log(err)
    console.log('[Seller_extractor]: New supplier created');
  })
}

async function createSupplierIfNotExist(page, categoryData){
  const supplier_data = await getter.supplierInfo(page)
  if(!supplier_data) return // if supplier is Amazon

  var supplierExist = await Supplier.findOne({name: supplier_data.name }).exec()
  if(supplierExist) {
    console.log('[Seller_extractor]: Supplier already exists');
    return
  }

  supplier_data.category = categoryData.category
  supplier_data.subcategory = categoryData.subcategory

  console.log(supplier_data);

  createSupplier(supplier_data)
}

module.exports = {
  createSupplierIfNotExist,
}
