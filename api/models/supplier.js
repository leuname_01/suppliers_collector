'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SupplierSchema = new Schema({
  name: {
    type: String,
    required: 'The supplier must have a name'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  category: { type: String, default: "" },
  subcategory: {type: String, default: "" },
  stats: {
    positive: {
      one_month: { type: Number, default: 0},
      three_months: { type: Number, default: 0},
      one_year: { type: Number, default: 0},
      lifetime: { type: Number, default: 0}
    },
    neutral: {
      one_month: { type: Number, default: 0},
      three_months: { type: Number, default: 0},
      one_year: { type: Number, default: 0},
      lifetime: { type: Number, default: 0}
    },
    negative: {
      one_month: { type: Number, default: 0},
      three_months: { type: Number, default: 0},
      one_year: { type: Number, default: 0},
      lifetime: { type: Number, default: 0}
    },
    count: {
      one_month: { type: Number, default: 0},
      three_months: { type: Number, default: 0},
      one_year: { type: Number, default: 0},
      lifetime: { type: Number, default: 0}
    }
  }
});

module.exports = mongoose.model('Supplier', SupplierSchema);
