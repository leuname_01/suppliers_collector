'use strict'

const puppeteer = require('puppeteer')
const explorer = require('../amazon/explorer')
const beep = require('beepbeep')

const prefix = "http://api.scraperapi.com?api_key=c5c0370a47d594f13398722912291b75&url="

// async function getSuppliers(){

    puppeteer
      .launch({slowMo: 500, headless: false, defaultViewport: { width: 1600, height: 900}})
      .then(async browser => {
        console.log("\x1b[32m", "[Seller_extractor]: ################ START ################");
        let pages = await browser.pages()
        const page = pages[0]
        // page.on('console', consoleObj => console.log("\x1b[32m", "[Seller_extractor]: "+consoleObj.text()));

        await page.goto('https://www.amazon.com/gp/site-directory?ref_=nav_shopall_btn') // departments page
        // await page.goto('https://www.amazon.com/gp/offer-listing/B06Y3QXSGX/ref=dp_olp_new_mbc?ie=UTF8&condition=new') // test
        // await explorer.extractSellers(page) // product's new page

        await explorer.setDeliveryZoneToUSA(page)

        console.log("[Seller_extractor]: Searching categories...");
        const selectedURLs = await explorer.getURLsFromDivOfCategories(page)
        console.log("[Seller_extractor]: Search finished");

        console.log("[Seller_extractor]: "+selectedURLs.length+" subcategories founded");

        for(const urlObject of selectedURLs){
          await page.goto(urlObject.url)
          console.log("[Seller_extractor]: Accessing to", urlObject.url);
          console.log("[Seller_extractor]: Category: %s | Subcategory: %s", urlObject.category, urlObject.subcategory);

          //
          var cardlinks = []

          switch (urlObject.category) {
            case 'Pet Supplies':
              await explorer.pets(page, urlObject)
            break;
            case 'Smart Home':
              cardlinks = await explorer.smartHome(page, urlObject)
              for(const cardlink of cardlinks){
                await explorer.searchSellerWithNewProduct(page, cardlink, urlObject)
              }
            break;
            case 'Home, Garden & Tools':
              await explorer.amazonHome(page, urlObject)
            break;
            case 'Toys, Kids & Baby':
              await explorer.babies(page, urlObject)
            break;
            default:

          }

        }


        console.log("\x1b[32m", "[Seller_extractor]: ################ FINISH ################");
        // await browser.close()
        // process.exit(0)
      })
      .catch(err => {
        console.log("\x1b[31m", "[Seller_extractor]: ################ ERROR ################");
        console.log("\x1b[31m", "[Seller_extractor]: "+err.stack)
        beep(2, 1000)
        process.exit(1)
      })
// }
