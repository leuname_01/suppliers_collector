const crud = require('../models/crud')

async function extractSellers(page, categoryData){
  console.log("\x1b[32m", "[Seller_extractor]: "+"Starting to extract sellers...");
  const paginatorExists = await page.$$eval('.a-last', el => !!el.length) // searching a paginator
  var hasNextPage = false

  do {
    if(hasNextPage) await page.$$eval('.a-last a', el => el[0].click())
    await extractSellersFromTable(page, categoryData)
    hasNextPage = paginatorExists && await page.$$eval('.a-last', el => !el[0].classList.contains('a-disabled'))

  } while(paginatorExists && hasNextPage)
}

async function extractSellersFromTable(page, categoryData) {
  const rowsIndex = await getRowsIndexBySellersWithNewProduct(page)
  console.log("\x1b[32m", "[Seller_extractor]: "+rowsIndex.length+" sellers with new product");
  const sellerLinks = await page.$$eval('#olpOfferList .olpOffer .olpSellerName a', el =>
    el.map( subEl =>
      subEl.getAttribute('href')
    )
  )

  for(var index of rowsIndex){
    if(sellerLinks[index]){
      var cont=0
      while (cont<3){
        try {
          var url = sellerLinks[index].includes("https://www.amazon.com")? sellerLinks[index] : "https://www.amazon.com"+sellerLinks[index]
          await page.goto(url) // see seller profile
          console.log("\x1b[33m", "[Seller_extractor]: Accessing to ", url);
          await crud.createSupplierIfNotExist(page, categoryData)
          cont=3
        } catch (e) {
          cont++
          if(cont==3){
            console.log("\x1b[31m", "[Seller_extractor]: ################ ERROR ################");
            console.log("\x1b[31m", "[Seller_extractor]:", e)
          }
        }
      }// end of while
      await page.goBack()
    } //end of if
  }
}

async function getRowsIndexBySellersWithNewProduct(page){
  return await page.$$eval('.olpConditionColumn', el =>
    el.map((el, index) => {
      if(el.innerText.includes("New") && !el.innerText.includes("Like New"))
        return index
      }).filter(el => el != undefined)
    )
}

async function getIndexFromCategoriesToNavigate(page){
  const categoriesArray = ['Garden'] // ['Home', 'Kitchen', 'Pet', 'Baby']
  const categories = await page.$$eval('.fsdDeptBox', el => el)
  const titles = await page.$$eval('.fsdDeptBox h2', elements => elements.map( element => element.innerText))
  return titles.map( (title, index) => {
    var isSelected = false
    categoriesArray.forEach( category => {
      if(title.match(new RegExp(category, 'i')))
        isSelected = true
    })
    return isSelected? index : null
  }).filter(el => el)

}

async function getURLsFromDivOfCategories(page){
  const indexes = await getIndexFromCategoriesToNavigate(page)
  var selectedURLs = await page.evaluate( indexes => {
    return indexes.map( index => {
      const divsOfLinks = [...document.querySelectorAll('.fsdDeptBox')]
      const categoryName = divsOfLinks[index].getElementsByTagName('h2')[0].innerText
      const allLinks = [...divsOfLinks[index].getElementsByTagName('a')].slice(25)
      return allLinks.map(a => {
        return {
          category: categoryName,
          subcategory: a.innerText,
          url: a.href}
      })//.filter(el => !['Lighting', 'Plugs', 'Cameras', 'Door Locks', 'Thermostats', 'TV & Video', 'Audio & Home Theater', 'Vacuums', 'Voice Assistants', 'Other Smart Solutions'].includes(el.subcategory))
    })
  }, indexes)

  return selectedURLs.flat()
}

async function searchSellerWithNewProduct(page, cardlink, urlObject){
  if(!cardlink.includes("https://www.amazon.com"))
    cardlink="https://www.amazon.com"+cardlink

  console.log("\x1b[33m", "[Seller_extractor]: Accessing to", cardlink);
  await page.goto(cardlink)
  const hasNewUsedLinks = await page.$$eval('#olp-upd-new-used a', el => el.map(subEl => subEl.getAttribute('href')))
  const hasNewLinks = await page.$$eval('#olp-upd-new a', el => el.map(subEl => subEl.getAttribute('href')))
  const hasAvailableLinks = await page.$$eval('#availability a', el => el.map(subEl => subEl.getAttribute('href')))

  if(hasNewUsedLinks.length)
    await page.goto('https://www.amazon.com'+hasNewUsedLinks[0])
  else
    if(hasNewLinks.length)
      await page.goto('https://www.amazon.com'+hasNewLinks[0])
    else
      if(hasAvailableLinks.length) await page.goto('https://www.amazon.com'+hasAvailableLinks[0])

  if(hasNewLinks.length || hasNewUsedLinks.length || hasAvailableLinks.length){
    await extractSellers(page, urlObject) // product's new page
  } else {
    console.log("\x1b[32m", "[Seller_extractor]: Sellers with used products skipped");
  }
}

async function setDeliveryZoneToUSA(page){
  console.log("[Seller_extractor]: Changing zone to USA");

  await page.$eval('.a-declarative', el=> el.click())
  await page.$eval('.a-dropdown-prompt', el=>el.click())
  await page.$$eval('.a-dropdown-item a', el=>el[234].click())
  await page.$eval('button[name=glowDoneButton]', el=> el.click())
}

async function smartHome(page, urlObject){
  const carrouselItemsTotal = await page.$eval('.a-carousel-card', el => parseInt(el.getAttribute('aria-setsize')))
  var cardlinks = []

  while(cardlinks.length != carrouselItemsTotal){
    const links = await page.$$eval('.a-carousel-card', cards =>
      cards.filter( card => card.getAttribute('aria-hidden') == "false")
      .map(card => card.children[0].getAttribute('href'))
    )
    cardlinks.push(...links)
    await page.$eval('.a-carousel-goto-nextpage', el => el.click())
  }

  return cardlinks
}

async function amazonHome(page, urlObject){
  await exploreProductsAmazonHome(page, urlObject)
  // const subCategoryUrls = await page.$$eval('.bxc-grid__image.bxc-grid__image--light a', el =>
  //   el.slice(1,17).map( el =>
  //     el.getAttribute('href')
  //   )
  // )
  // var cardlinks = []
  // for(const url of subCategoryUrls){
  //   await page.goto("https://www.amazon.com"+url)
  //   if(!['Home Décor', 'Kitchen & Dining', 'Bed & Bath', 'Garden & Outdoor'].includes(urlObject.subcategory)){
  //     const subsubCategoryUrls = await page.$$eval('.bxc-grid__image.bxc-grid__image--light a', el =>
  //       el.slice(5,17).map( el =>
  //         el.getAttribute('href')
  //       )
  //     )
  //     if(subsubCategoryUrls.length){
  //       for(const subUrl of subsubCategoryUrls){
  //         await page.goto("https://www.amazon.com"+ subUrl)
  //         await exploreProductsAmazonHome(page, urlObject)
  //
  //       }
  //     }
  //   } else {
  //     await exploreProductsAmazonHome(page, urlObject)
  //   }
  // }
}

async function exploreProductsAmazonHome(page, urlObject){
  var links = []
  links = await page.$$eval('.s-color-twister-title-link', el => el.map(el => el.getAttribute('href'))) // to select product card to navigate

  var nextPageUrl = await page.$$eval('#pagnNextLink', el => el.length && el[0].getAttribute('href'))

  for(url of links){
    await searchSellerWithNewProduct(page, url, urlObject)
  }

  if(!nextPageUrl) return

  await page.goto("https://www.amazon.com"+nextPageUrl)

  await productCardsWithPaginator(page, urlObject)
}

async function productCardsWithPaginator(page, urlObject){
  const paginatorExists = await page.$$eval('.a-last', el => !!el.length) // searching a paginator
  var hasNextPage = false

  do {
    if(paginatorExists)
      nextPageUrl = await page.$$eval('.a-last a', el => el.length && el[0].getAttribute('href'))

    links = await page.$$eval('h2 .a-link-normal.a-text-normal', el => el.map(el => el.getAttribute('href')))

    for(url of links){
      await searchSellerWithNewProduct(page, url, urlObject)
    }

    if(nextPageUrl) await page.goto("https://www.amazon.com"+nextPageUrl)

  } while(paginatorExists && nextPageUrl)
}

async function babies(page, urlObject){
  await productCardsWithPaginator(page, urlObject)
}

async function pets(page, urlObject){
  await exploreProductsAmazonHome(page, urlObject)
}


module.exports = {
  getURLsFromDivOfCategories,
  searchSellerWithNewProduct,
  setDeliveryZoneToUSA,
  smartHome,
  amazonHome,
  babies,
  pets
}
